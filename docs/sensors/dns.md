---
#### Description
Monitors a DNS server (Domain Name Service), resolves a domain name, and compares it to an IP address.

---
#### Configuration
**Timeout**: If the reply takes longer than this value the request is aborted and an error message is triggered.  
**Port**:  Enter the port on which the DNS service of the parent device is running.   
**Domain**:  Enter a DNS name to resolve.  
**Filter**:  Enter comma separated strings to look for in the result. i.e. 192.168.1.0,192.168.2.0 or "
            "MX=10 mx.example.com,MX=20 mx2.example.com.  "
            "These values will be compared to the last message shown in the sensor."
            " If it doesn't match the resolved record, the sensor will "
            "go into an error state, if left blank the dns record will not be checked.  
**Query Type**:  Specify the type of query that the sensor will send to the DNS server.

---
![DNS](../img/sensor_dns.png){: align=left }
