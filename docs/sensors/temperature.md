---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor used to monitor the temperature of the Python probe.

---
#### Configuration
**Choose between Celsius or Fahrenheit display**:  Choose whether you want to return the value in Celsius or Fahrenheit. When changed after sensor is created, the unit will disappear in the web interface.  

---
![Temperature](../img/sensor_temperature.png){: align=left }
