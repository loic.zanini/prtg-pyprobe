import asyncio

import asynctest
import pytest
from botocore.exceptions import ClientError

import prtg_pyprobe.sensors.sensor_s3_bucket_total as module
from prtg_pyprobe.sensors.sensor import SensorBase


def task_data():
    return {
        "sensorid": "1234",
        "host": "127.0.0.1",
        "kind": "mps3buckettotal",
        "aws_access_key": "1123124",
        "aws_secret_key": "jkh2089",
    }


class TestS3TotalSensorProperties:
    def test_sensor_s3_total_base_class(self, s3_total_sensor):
        assert type(s3_total_sensor).__bases__[0] is SensorBase

    def test_sensor_s3_total_name(self, s3_total_sensor):
        assert s3_total_sensor.name == "S3 Bucket Total"

    def test_sensor_s3_total_kind(self, s3_total_sensor):
        assert s3_total_sensor.kind == "mps3buckettotal"

    def test_sensor_s3_total_definition(self, s3_total_sensor):
        assert s3_total_sensor.definition == {
            "description": "Sensor for monitoring the total amount of your S3 buckets.",
            "groups": [
                {
                    "caption": "S3 Bucket Sensor Settings",
                    "fields": [
                        {"caption": "AWS Access Key ID", "name": "aws_access_key", "type": "edit"},
                        {"caption": "AWS Secret Key", "name": "aws_secret_key", "type": "password"},
                    ],
                    "name": "s3_bucket_total",
                }
            ],
            "help": "This returns the total amount of buckets in your AWS account.",
            "kind": "mps3buckettotal",
            "name": "S3 Bucket Total",
            "tag": "mps3buckettotal",
        }


@pytest.mark.asyncio
class TestS3TotalWork:
    @asynctest.patch("aioboto3.resource")
    async def test_sensor_s3_total(self, aioboto_mock, s3_total_sensor):
        buckets = asynctest.MagicMock()
        buckets.__aiter__.return_value = ["bucket1", "bucket2", "bucket3"]

        aioboto_mock.return_value.__aenter__.return_value.buckets.all.return_value = buckets

        s3_total_queue = asyncio.Queue()

        await s3_total_sensor.work(task_data=task_data(), q=s3_total_queue)

        queue_result = await s3_total_queue.get()

        aioboto_mock.assert_called_once_with("s3", aws_access_key_id="1123124", aws_secret_access_key="jkh2089")
        assert queue_result["message"] == "Your AWS account has 3 buckets."
        assert {
            "customunit": "buckets",
            "kind": "Custom",
            "mode": "integer",
            "name": "Total Buckets",
            "value": 3,
        } in queue_result["channel"]

    @asynctest.patch("aioboto3.resource", side_effect=ClientError({}, {}))
    async def test_sensor_s3_total_boto_error(self, aioboto_mock, sensor_exception_message, s3_total_sensor, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        s3_total_queue = asyncio.Queue()
        await s3_total_sensor.work(task_data=task_data(), q=s3_total_queue)
        queue_result = await s3_total_queue.get()

        aioboto_mock.assert_called_once_with("s3", aws_access_key_id="1123124", aws_secret_access_key="jkh2089")

        sensor_exception_message["message"] = "S3 bucket sensor failed. See log for details"

        assert queue_result == sensor_exception_message
        logging_mock.exception.assert_called_once_with("The S3 bucket sensor couldn't contact AWS properly")
