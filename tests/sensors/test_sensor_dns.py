import asyncio

import pytest
from dns.asyncresolver import Resolver
from dns.exception import DNSException

from prtg_pyprobe.sensors import sensor_dns as module
from prtg_pyprobe.sensors.sensor import SensorBase
from prtg_pyprobe.sensors.sensor_dns import NoMatchException
from tests.conftest import dns_answer


def dns_answer_no_match_exception(*args, **kwargs):
    raise NoMatchException("test", "test")


def dns_answer_dns_exception(*args, **kwargs):
    raise DNSException


def task_data(qtype="A", qfilter=""):
    return {
        "sensorid": "1234",
        "host": "8.8.8.8",
        "domain": "paessler.com",
        "filter": qfilter,
        "type": qtype,
        "port": "53",
        "timeout": "5",
        "kind": "mpdns",
    }


class TestDNSSensorProperties:
    def test_sensor_dns_base_class(self, dns_sensor):
        assert type(dns_sensor).__bases__[0] is SensorBase

    def test_sensor_dns_name(self, dns_sensor):
        assert dns_sensor.name == "DNS"

    def test_sensor_dns_kind(self, dns_sensor):
        assert dns_sensor.kind == "mpdns"

    def test_sensor_dns_definition(self, dns_sensor):
        assert dns_sensor.definition == {
            "description": "Monitors a DNS server (Domain Name Service), resolves a "
            "domain name, and compares it to an IP address",
            "groups": [
                {
                    "caption": "DNS Specific",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 5,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 1,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Port",
                            "default": 53,
                            "help": "Enter the port on which the DNS service of " "the parent device is running.",
                            "maximum": 65535,
                            "minimum": 1,
                            "name": "port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Domain",
                            "help": "Enter a DNS name to resolve.",
                            "name": "domain",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Filter",
                            "help": "Enter comma separated strings to look for in "
                            "the result. i.e. 192.168.1.0,192.168.2.0 or "
                            "MX=10 mx.example.com,MX=20 mx2.example.com.  "
                            "These values will be compared to the last "
                            "message shown in the sensor. If it doesn't "
                            "match the resolved record, the sensor will "
                            "go into an error state, if left blank the "
                            "dns record will not be checked",
                            "name": "filter",
                            "type": "edit",
                        },
                        {
                            "caption": "Query Type",
                            "default": "A",
                            "help": "Specify the type of query that the sensor " "will send to the DNS server.",
                            "name": "type",
                            "options": {
                                "A": "Host address IPv4 (A)",
                                "AAAA": "Host address IPv6 (AAAA)",
                                "CNAME": "Canonical name for an alias " "(CNAME)",
                                "MX": "Mail exchange (MX)",
                                "NS": "Authoritative name server (NS)",
                                "SOA": "Start of a zone of authority " "marker (SOA)",
                                "SRV": "Service Record",
                                "TXT": "Text Record",
                            },
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "dns_specific",
                }
            ],
            "help": "The DNS sensor monitors a Domain Name Service (DNS) server. It "
            "resolves a domain name and compares it to a given IP address.",
            "kind": "mpdns",
            "name": "DNS",
            "tag": "mpdnssensor",
        }


@pytest.mark.asyncio
class TestDNSSensorWork:
    async def test_sensor_work_dns_error(self, dns_sensor, monkeypatch, sensor_exception_message, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Resolver, "resolve", dns_answer_dns_exception)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(), q=dns_q)
        out = await dns_q.get()
        sensor_exception_message["message"] = "DNS check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("Error in dnspython.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_work_nomatch_error(self, dns_sensor, monkeypatch, sensor_exception_message, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Resolver, "resolve", dns_answer_no_match_exception)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(), q=dns_q)
        out = await dns_q.get()
        sensor_exception_message[
            "message"
        ] = "Filtered value 'test' not found in last message 'test'. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("Filter did not match!")
        logging_mock.exception.assert_called_once()

    async def test_sensor_dns_work_success_a_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: A for paessler.com. Result: 1.2.3.4, "
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_aaaa_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="AAAA"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: AAAA for paessler.com. Result: 1.2.3.4, "
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_cname_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="CNAME"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: CNAME for paessler.com. Result: mytarget, "
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_mx_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="MX"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: MX for paessler.com. Result: mypref: myex"
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_ns_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="NS"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: NS for paessler.com. Result: mytarget, "
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_soa_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="SOA"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert (
            out["message"] == "Query Type: SOA for paessler.com. Result: NS: mname, "
            "TECH: rname, S/N: serial, Refresh: 1.0 min, Expire: 1.0 min"
        )
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_srv_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="SRV"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: SRV for paessler.com. Result: mytarget, "
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_success_txt_rec(self, dns_sensor, monkeypatch):
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="TXT"), q=dns_q)
        out = await dns_q.get()
        assert out["sensorid"] == "1234"
        assert out["message"] == "Query Type: TXT for paessler.com. Result: string1string2"
        assert len(out["channel"]) == 1

    async def test_sensor_dns_work_fail_filter_a_rec(self, dns_sensor, monkeypatch, mocker):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Resolver, "resolve", dns_answer)
        dns_q = asyncio.Queue()
        await dns_sensor.work(task_data=task_data(qtype="A", qfilter="not, matching"), q=dns_q)
        out = await dns_q.get()
        assert out == {
            "code": 1,
            "error": "Exception",
            "message": "Filtered value 'not' not found in last message 'Query Type: A for "
            "paessler.com. Result: 1.2.3.4, '. See log for details",
            "sensorid": "1234",
        }
        logging_mock.exception.assert_called_with("Filter did not match!")
        logging_mock.exception.assert_called_once()
